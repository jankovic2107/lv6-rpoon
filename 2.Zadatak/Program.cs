﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.Zadatak
{
    class Program
    {
        private static IAbstractIterator iterator;
        static void Main(string[] args)
        {
            Box box = new Box();
            box.AddProduct(new Product("Glass", 19.99));
            box.AddProduct(new Product("Guitar", 1500));
            box.AddProduct(new Product("Pottery", 100));

            iterator = box.GetIterator();
            while (iterator.IsDone != true)
            {
                Console.WriteLine(iterator.Current.ToString());
                iterator.Next();
                Console.WriteLine();
            }
        }
    }
}
