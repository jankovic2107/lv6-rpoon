﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger1 = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger = new FileLogger(MessageType.ERROR | MessageType.WARNING, @"C: \Users\Ivan Jankovic\Desktop\RPOON-Lv6.txt");
            logger1.SetNextLogger(fileLogger);
            logger1.Log("Nesto", MessageType.WARNING);
            logger1.Log("samoto", MessageType.INFO);
            logger1.Log("inistavise", MessageType.ERROR);
        }
    }
}
