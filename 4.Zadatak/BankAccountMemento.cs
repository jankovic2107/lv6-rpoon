﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.Zadatak
{
    class BankAccountMemento
    {
        public string OwnerName { get; private set; }
        public string OwnerAddress { get; private set; }
        public decimal Balance { get; private set; }

        public BankAccountMemento(string OwnerName, string OwnerAddress, decimal Balance)
        {
            this.OwnerName = OwnerName;
            this.OwnerAddress = OwnerAddress;
            this.Balance = Balance;
        }
    }
}
