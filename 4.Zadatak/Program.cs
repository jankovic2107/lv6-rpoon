﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount1 = new BankAccount("Marko", "Nekog Nekoga 19", 199.89M);
            BankAccount bankAccount2 = new BankAccount("David", "njeg njega 100", 1500.234M);
            BankAccountMemento bankAccountMemento1 = bankAccount1.StoreState();
            BankAccountMemento bankAccountMemento2 = bankAccount2.StoreState();

            bankAccount2.UpdateBalance(19.64M);
            Console.WriteLine(bankAccount2.ToString());
            bankAccount2.RestoreState(bankAccountMemento2);
            Console.WriteLine(bankAccount2.ToString());
        }
    }
}
