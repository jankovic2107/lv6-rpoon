﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDoItem toDoItem1 = new ToDoItem("Songs", "Freaks", DateTime.Now);
            ToDoItem toDoItem2 = new ToDoItem("Songs", "Starlight", DateTime.Now);
            CareTaker careTaker = new CareTaker();
            
            careTaker.StoreState(toDoItem2.StoreState());
            careTaker.StoreState(toDoItem1.StoreState());

            Console.WriteLine(toDoItem2.ToString());
            toDoItem2.RestoreState(careTaker.previousState());
            Console.WriteLine(toDoItem2.ToString());
            toDoItem2.RestoreState(careTaker.previousState());
            Console.WriteLine(toDoItem2.ToString());

            

        }
    }

}
