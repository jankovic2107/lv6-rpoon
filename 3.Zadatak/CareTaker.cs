﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3.Zadatak
{
    class CareTaker
    {
        private Stack<Memento> previousStates;
        public CareTaker()
        {
            this.previousStates = new Stack<Memento>();
        }
        public Memento previousState()
        {
            if(previousStates.Count == 0)
            {
                return null;
            }
            return previousStates.Pop();
        }
        public void StoreState(Memento state)
        {
            previousStates.Push(state);
        }
    }
}
