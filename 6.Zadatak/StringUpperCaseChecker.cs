﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class StringUpperCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasUpper = false;
            foreach (char c in stringToCheck)
            {
                if (char.IsUpper(c))
                {
                    hasUpper = true;
                }
            }
            return hasUpper;
        }
    }
}
