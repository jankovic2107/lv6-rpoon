﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class StringDigitChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasDigit = false;
            foreach(char c in stringToCheck)
            {
                if (char.IsDigit(c))
                {
                    hasDigit = true;
                }
            }
            return hasDigit;
        }
    }
}
