﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class StringLowerCaseChecker : StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            bool hasLower = false;
            foreach (char c in stringToCheck)
            {
                if (char.IsLower(c))
                {
                    hasLower = true;
                }
            }
            return hasLower;
        }
    }
}
