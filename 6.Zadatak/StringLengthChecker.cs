﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.Zadatak
{
    class StringLengthChecker : StringChecker
    {
        private int minLenght;
        public int MinLenght { get; private set; }
        public StringLengthChecker(int minLenght)
        {
            this.minLenght = minLenght;
        }
        protected override bool PerformCheck(string stringToCheck)
        {
            bool isLongEnough = false;
            if(stringToCheck.Length >= this.minLenght)
            {
                isLongEnough = true;
            }
            return isLongEnough;
        }
    }
}
